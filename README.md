# Red Hat LaTeX/Beamer template

The goal of this repo is to create a functioning LaTeX/Beamer template that is able to produce slides that are not distinguishable from the ones created with the official template.

## Demo

The PDF result of the demo is [here](http://lacrosse.redhat.com/~flocati/demo.pdf).

## Packages for Fedora and RHEL

* copr: [texlive-beamerthemeredhat](https://copr.devel.redhat.com/coprs/pkotvan/texlive-beamerthemeredhat/)

## Requirements

* beamer (rpm: `texlive-beamer`)
* xelatex (rpm: `texlive-xetex-bin`)
* overpass-font (rpm: `overpass-fonts`)

## To Do list

* Work nicely with both 4:3 and 16:9 presentations
* Support all internal layout supported by the official presentation
* Add a way to use different images for cover, dividers, and thanks pages
* Understand better the images license

## License

* src/redhat.sty is under GPLv3+
* images in src/* have been taken from the PnT OTP presentation but display no license

## Thanks

* kzak for the [Original work](http://git.engineering.redhat.com/git/users/kzak/rh-beamer-theme.git/) that I used as a starting point
* pkotvan for the SPEC file
