%global owner flocati
%global reponame redhat-beamer-template
%global texname beamer-themeredhat
%global commit0 ea3d2d0e5d8e3ef57ce027794e306913e8c4997c
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%{!?_texdir: %global _texdir %{_datadir}/texlive}

Name:           tex-%{texname}
Version:        0.0
Release:        3.%{?commit0:git%{shortcommit0}}%{?dist}
Summary:        Red Hat beamer template

License: GPLv3+ and Commercial
URL: https://gitlab.cee.redhat.com/flocati/redhat-beamer-template

BuildArch: noarch
BuildRequires: coreutils

%if %{defined rhel}
Requires: texlive-scheme-basic
Requires: texlive-zapfding
%endif

%if %{defined fedora}
Requires: texlive-scheme-minimal
%endif

Requires: texlive-xetex
Requires: texlive-xetex-def
Requires: texlive-beamer
Requires: texlive-euenc
Requires: overpass-fonts

Source0: https://gitlab.cee.redhat.com/%{owner}/%{reponame}/repository/archive.tar.gz?ref=%{commit0}#/%{name}-%{shortcommit0}.tar.gz
Obsoletes: texlive-beamerredhat

%description
Red Hat beamer temaplate.

%prep
%setup -n %{reponame}-%{commit0}-%{commit0}
rm src/*.svg

%install
mkdir -p %{buildroot}/%{_texdir}/texmf-dist/tex/latex/%{texname}
cp src/* %{buildroot}/%{_texdir}/texmf-dist/tex/latex/%{texname}

%post
mkdir -p /var/run/texlive
touch /var/run/texlive/run-texhash
touch /var/run/texlive/run-mtxrun
:

%postun
if [ $1 == 1 ]; then
  mkdir -p /var/run/texlive
  touch /var/run/texlive/run-texhash
else
  %{_bindir}/texhash 2> /dev/null
fi
:

%posttrans
if [ -e /var/run/texlive/run-texhash ] && [ -e %{_bindir}/texhash ]; then %{_bindir}/texhash 2> /dev/null; rm -f /var/run/texlive/run-texhash; fi
if [ -e /var/run/texlive/run-mtxrun ]; then export TEXMF=/usr/share/texlive/texmf-dist; export TEXMFCNF=/usr/share/texlive/texmf-dist/web2c; export TEXMFCACHE=/var/lib/texmf; %{_bindir}/mtxrun --generate &> /dev/null; rm -f /var/run/texlive/run-mtxrun; fi
:

%files
%doc README.md
%dir %{_texdir}/texmf-dist/tex/latex/%{texname}
%{_texdir}/texmf-dist/tex/latex/%{texname}/box.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/cover.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/divider.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/background.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/redhat.sty
%{_texdir}/texmf-dist/tex/latex/%{texname}/redhat.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/redhat-w.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/social_facebook.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/social_gplus.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/social_linkedin.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/social_twitter.png
%{_texdir}/texmf-dist/tex/latex/%{texname}/social_youtube.png

%changelog
* Tue Nov 01 2016 Fabio Alessandro Locati <fale@redhat.com> - 0.0-3.b79d1748
- More social stuff
- Rename package and change location

* Tue Nov 01 2016 Fabio Alessandro Locati <fale@redhat.com> - 0.0-2.dd9b96a7
- Add social stuff

* Tue Oct 18 2016 Peter Kotvan <pkotvan@redhat.com> - 0.0-1.6af22575
- Create texlive-beamer-redhat package.
