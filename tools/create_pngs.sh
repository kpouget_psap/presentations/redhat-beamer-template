#!/bin/bash
BASEDIR="$( cd "$( dirname "$( dirname "${BASH_SOURCE[0]}")" )" && pwd )"

# RH logos
inkscape -z -e ${BASEDIR}/src/redhat.png -w 200 -h 64 ${BASEDIR}/src/redhat.svg
inkscape -z -e ${BASEDIR}/src/redhat-w.png -w 200 -h 64 ${BASEDIR}/src/redhat-w.svg

# Social
inkscape -z -e ${BASEDIR}/src/social_facebook.png -w 60 -h 60 ${BASEDIR}/src/social_facebook.svg
inkscape -z -e ${BASEDIR}/src/social_gplus.png -w 60 -h 60 ${BASEDIR}/src/social_gplus.svg
inkscape -z -e ${BASEDIR}/src/social_linkedin.png -w 60 -h 60 ${BASEDIR}/src/social_linkedin.svg
inkscape -z -e ${BASEDIR}/src/social_twitter.png -w 60 -h 60 ${BASEDIR}/src/social_twitter.svg
inkscape -z -e ${BASEDIR}/src/social_youtube.png -w 60 -h 60 ${BASEDIR}/src/social_youtube.svg
